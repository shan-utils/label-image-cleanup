#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import matplotlib.pyplot as plt
from scipy.ndimage.measurements import label
import time
import nibabel as nib


image = np.array([[0, 0, 0, 0, 0],
                  [0, 1, 0, 0, 0],
                  [0, 0, 0, 1, 1],
                  [0, 1, 0, 1, 1],
                  [0, 1, 0, 0, 0]])
image = nib.load('../scripts/seg2.nii.gz').get_data() == 12
comps, num = label(image)

def show(subs):
    plt.figure()
    for i, sub in enumerate(subs):
        plt.subplot(1, len(subs), i + 1)
        plt.imshow(sub)

# normal way
start = time.time()
subs1 = [comps == (i+1) for i in range(num)]
print('list', time.time() - start)

# ind
start = time.time()
subs2 = np.zeros((num + 1, image.size))
subs2[comps.flatten(), np.arange(image.size)] = 1
subs2 = subs2.reshape(-1, *image.shape)[1:, ...]
print('ind', time.time() - start)

# eye
start = time.time()
tmp = np.eye(num + 1)
subs3 = tmp[comps.flatten()].T
subs3 = subs3.reshape(-1, *image.shape)[1:, ...]
print('eye', time.time() - start)

for sub1, sub2, sub3 in zip(subs1, subs2, subs3):
    assert np.array_equal(sub1, sub2)
    assert np.array_equal(sub1, sub3)

# show(subs1)
# show(subs2)
# show(subs3)
# plt.show()
