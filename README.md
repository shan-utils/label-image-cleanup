# Label Image Clean Up

Remove isolated pixels in a label image and fill holes.

### Installation

```bash
$ pip install git+git@https://gitlab.com/shan-utils/label-image-cleanup.git
```

### Usage

See [dcumentation](https://shan-utils.gitlab.io/label-image-cleanup) for more details.

```bash
$ cleanup_label_image.py -i input.nii.gz -o output.nii.gz
```
